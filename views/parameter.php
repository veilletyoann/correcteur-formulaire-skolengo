<?php
/**
     * Fichier read.php - Correcteur de formulaire pour l'ENT ECLAT (KOSMOS)
     * Réalisé par Y. VEILLET
     * Développement Initial par Fabien CABANEL
     * Architecture MVC inspirée du LIVE CODING : https://nouvelle-techno.fr/actualites/live-coding-introduction-au-mvc-en-php
*/

// Appel de la méthode
list($tableau,$tableau_reponses_differentes,$tmp_name)=Parameter::file_processing();

// Définition des variables
$nbcolonnes = count ($tableau[0]);
//$nblignes = count($tableau)-1;
$nblignes = count($tableau);
$nomfichier=$tmp_name;
?>
<form name="form" method="POST" action="index.php?action=correct">

	<input type="hidden" name="nomfichier" value="<?php echo $nomfichier;?>" />
	
	<table class="table-parameter" border='1'>
	<tr>
		<td class='entete width-25'>Questions</td>
		<td class='entete'>
			<div style="float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Attribution des Points</div>
			<div style="float:center;">Réponses obtenues</div>
		</td>
	</tr>

<?php
	
	for ($j = 3; $j < $nbcolonnes-1; $j++) 
	{
		//Affiche l'intitulé des questions
		echo '<tr><td class="center">';
		echo $tableau[0][$j];echo'</td>';
		
		//Affiche le tableau contenant les réponses, les points, la ligne Autre
		echo '<td class="left">'; 
		for ($i=1; $i<$nblignes ; $i++) 
		{
				if (($tableau_reponses_differentes[$i][$j] != 'DEJA_DIT') AND ($tableau[$i][$j] !=' '))
			{
			
			//Affiche les réponses différentes données par les élèves
			?>
			<input style='margin:2px;' type="button" value="-1" name="moins1" onClick="Points('Moins',document.form.<?php echo ("nombre" . $i . "_" . $j); ?>,1,'<?php echo $i.'_'.$j; ?>');">
			<input style='margin:2px;' type="button" value="-0,5" name="moins2" onClick="Points('Moins',document.form.<?php echo ("nombre" . $i . "_" . $j); ?>,0.5,'<?php echo $i.'_'.$j; ?>');">
			<input class="center bold" readonly='readonly' type="text" size="1" name="<?php echo ("nombre" . $i . "_" . $j); ?>" value="0" >
			<input style='margin:2px;' type="button" value="+0,5" name="plus1" onClick="Points('Plus',document.form.<?php echo ("nombre" . $i . "_" . $j); ?>,0.5,'<?php echo $i.'_'.$j; ?>');">
			<input style='margin:2px;' type="button" value="+1" name="plus2" onClick="Points('Plus',document.form.<?php echo ("nombre" . $i . "_" . $j); ?>,1,'<?php echo $i.'_'.$j; ?>');">
			<?php
			
			echo '<input type="hidden" name="reponse'.$i.'_'.$j.'" value ="'.$tableau_reponses_differentes[$i][$j].'"><span id="span_reponse'.$i.'_'.$j.'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$tableau_reponses_differentes[$i][$j].'</span>';
			
			
			echo '<div style="clear:both"></div>';
			}
		}
		// Affichage de la ligne Autre	
		?>
		<input style='margin:2px;' type="button" value="-1" name="moins1" onClick="Points('Moins',document.form.<?php echo ("nombre" . $i . "_" . $j); ?>,1,'<?php echo $i.'_'.$j; ?>');">
		<input style='margin:2px;' type="button" value="-0,5" name="moins2" onClick="Points('Moins',document.form.<?php echo ("nombre" . $i . "_" . $j); ?>,0.5,'<?php echo $i.'_'.$j; ?>');">
		<input class="center bold" readonly='readonly' type="text" size="1" name="<?php echo ("nombre" . $i . "_" . $j); ?>" value="0" >
		<input style='margin:2px;' type="button" value="+0,5" name="plus1" onClick="Points('Plus',document.form.<?php echo ("nombre" . $i . "_" . $j); ?>,0.5,'<?php echo $i.'_'.$j; ?>');">
		<input style='margin:2px;' type="button" value="+1" name="plus2" onClick="Points('Plus',document.form.<?php echo ("nombre" . $i . "_" . $j); ?>,1,'<?php echo $i.'_'.$j; ?>');">
				
		<?php
		echo '<input type="hidden" name="reponse'.$i.'_'.$j.'" value="choix_perso" /><span id="span_reponse'.$i.'_'.$j.'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Autre : </span><input class="width-25" type="text" name="choix_perso'.$j.'" />';
		echo '</tr>';
	}
?>
		<tr>
			<td colspan="2" class='entete'>
				<input class='submit' id="submit" type="submit" value="CORRIGER">
				<input class='submit' id="submit" type="button" onClick="redir('index.php?action=load','Yes');"; value="ACCUEIL">
			</td>
		</tr>
	</table>
	<br/>
	<table class="table-parameter">
    <tr>
        <td>
			<p><strong><u>Remarque </u></strong>:</p>
			<p>Pour une question avec réponses multiples (case à cocher), les réponses seront séparées par le caractère <span style="color:#FF0000"><strong>|</strong></span>  </p>
			<p><u>Exemple</u> : Si la réponse exacte est :  b <u>ET</u> c, il faudra mettre des points sur la ligne : <strong> b|c </strong></p>
			<p>Il est possible de mettre des points (moins...) sur la réponse partielle b et sur la réponse partielle c.</p>
		</td>
	</tr>
	</table>
</form>