<?php
/**
     * Fichier correction.php - Correcteur de formulaire pour l'ENT ECLAT (KOSMOS)
     * Réalisé par Y. VEILLET
     * Développement Initial par Fabien CABANEL
     * Architecture MVC inspirée du LIVE CODING : https://nouvelle-techno.fr/actualites/live-coding-introduction-au-mvc-en-php
*/

// Appel de la méthode
list($tableau,$nomfichier,$nblignes, $nbcolonnes,$points_complets,$ptmax,$total_points)=Correct::correct_csv();
$ptmax=explode('@@',$ptmax);
?>
	<form class="center" name="form_download" method="post" action="index.php?action=download" enctype="multipart/form-data" >
	<input type="hidden" name="filename" value="<?php echo $nomfichier; ?>">
	<input class="submit no-print " id="submit" type="submit" value="EXPORT .csv">
	<input class="submit no-print " id="submit" type="button" onClick="redir('index.php?action=load','Yes');"; value="ACCUEIL">
	<input class="submit no-print " id="submit" type="button" onClick="ExportPdf('<?php echo $nomfichier; ?>');" value="EXPORT .pdf">
	</form>
<?php

//afficher le tableau en mettant le total de points de l'utilisateur, et en mettant les couleurs
	$couleur='white';
	echo '<table id="table-pdf-printable" class="table-correct" border="1">';
	for($i=0;$i<=$nblignes+1;$i++) 
	{ 
	echo '<tr>';
	for($j=0;$j<$nbcolonnes;$j++) 
		{
		if (($j >= 3) && ($i!=0) && ($i<$nblignes)) 
			{ 
			if ( $points_complets[$i][$j] <= 0 ) { $couleur='red'; }
			//if ( ( $points_complets[$i][$j] > 0 ) AND ( $points_complets[$i][$j] < ${'ptmax'.$j} ) ) { $couleur='orange'; }
			//if ( ( $points_complets[$i][$j] > 0 ) AND ( $points_complets[$i][$j] == ${'ptmax'.$j} ) ) { $couleur='green'; }
			$k=$j-2;
			//var_dump($points_complets[$i][$j]);
			//var_dump($ptmax[$k]);

			if ( ( $points_complets[$i][$j] > 0 ) AND ( $points_complets[$i][$j] < $ptmax[$k]) )  { $couleur='orange'; }
			if ( ( $points_complets[$i][$j] > 0 ) AND ( $points_complets[$i][$j] == $ptmax[$k] ) ) { $couleur='green'; }
			}
		if (($i==0) || ($i==$nblignes+1)) {$couleur='grey';}
		if ($i==$nblignes) {$couleur='springgreen';}
		if ($i==$nblignes+1) {$couleur='grey';}
		
		// Couleurs Colonne Points
		if (($j==1) && (($i>0) && ($i<$nblignes+1))) 
		{
			if($tableau[$i][$j]<($total_points/2))
			{$couleur_span='CircleRed';}	
			if($tableau[$i][$j]==($total_points/2))
			{$couleur_span='CircleOrange';}	
			if($tableau[$i][$j]>($total_points/2))
			{$couleur_span='CircleGreen';}	
		}
		else
		{$couleur_span='';}
		
		
		echo '<td class=td-'.$couleur.'><span class='.$couleur_span.'>';
		$couleur='white';
		echo $tableau[$i][$j];
		echo '</span></td>';
		}
	echo '</tr>';
	}
	echo '</table>';
?>