<?php
/**
     * Fichier load.php - Correcteur de formulaire pour l'ENT ECLAT (KOSMOS)
     * Réalisé par Y. VEILLET
     * Développement Initial par Fabien CABANEL
     * Architecture MVC inspirée du LIVE CODING : https://nouvelle-techno.fr/actualites/live-coding-introduction-au-mvc-en-php
*/
?>
<table class="table-load">
	<tr>
		<td>
			<p><u>ETAPE 1</u> : Exporter le fichier .zip depuis l'ENT ECLAT</p>
			<p><img src="public/images/export.png"></p>
			<p><img src="public/images/attention.png"></p>
			<p>Le caractère <span style="color:#FF0000"><strong>; (point-virgule)</strong></span> ne doit être ni dans une question, ni dans les réponses !</p>
		</td>
	</tr>
</table>
<br/>
<table class="table-load">
    <tr>
        <td>
			    <p><u>ETAPE 2</u></p>
				<form id="form_load_zip" name="form_load_zip" method="post" action="index.php?action=parameter" onsubmit="return controle_form();" enctype="multipart/form-data" >
					<div id="depose">Déposer le fichier .zip / .csv ou cliquer pour le sélectionner</div>
					<input id='zip_file' type="file" name="zip_file" accept="application/zip,.csv" required style="display:none"/>
					</div>
					<div class="bloc" id="preview"></div>
					<div id="action_buttons" style="display:none;float:right;">
					<input class="submit" type="button" value="Changer de fichier" onclick="redir('index.php?action=load','No')" >
					<input class="submit" type="submit" value="Envoyer le fichier">
					</div>
				</form>
				<script type="text/javascript" src="public/js/DragAndDrop.js"></script>            
        </td>
    </tr>  
</table>