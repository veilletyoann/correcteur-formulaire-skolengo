<?php
/**
     * Fichier header.php - Correcteur de formulaire pour l'ENT ECLAT (KOSMOS)
     * Réalisé par Y. VEILLET
     * Développement Initial par Fabien CABANEL
     * Architecture MVC inspirée du LIVE CODING : https://nouvelle-techno.fr/actualites/live-coding-introduction-au-mvc-en-php
*/
?>

<head>
	<meta charset="UTF-8">
    <title>Correcteur de Formulaire</title>
	<link href="public/css/style.css" rel="stylesheet">
	<link href="public/css/kendo.common-material.min.css" rel="stylesheet">
	<link href="public/css/kendo.material.min.css" rel="stylesheet">
	<link href="public/css/dd.css" rel="stylesheet">
	
	<script type="text/javascript" src="public/js/script.js"></script>
	<script type="text/javascript" src="public/js/jquery.min.js"></script>
	<script type="text/javascript" src="public/js/jszip.min.js"></script>
	<script type="text/javascript" src="public/js/kendo.all.min.js"></script>
	
</head>