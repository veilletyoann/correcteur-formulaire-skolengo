<?php
/**
     * Fichier default.php - Correcteur de formulaire pour l'ENT ECLAT (KOSMOS)
     * Réalisé par Y. VEILLET
     * Développement Initial par Fabien CABANEL
     * Architecture MVC inspirée du LIVE CODING : https://nouvelle-techno.fr/actualites/live-coding-introduction-au-mvc-en-php
*/
?>
<!DOCTYPE html>
<html>
<?php include_once 'header.php'; ?>

<body>
	<!--
	<p class="no-print"><img src="public/images/logo.png"></p>
	-->
	
	<hr class="hr-<?php echo $_GET["action"]; ?>">
    <h1>Correcteur de Formulaire</h1>
	<hr class="hr-<?php echo $_GET["action"]; ?>">
    <div class="wrapper">
	<?= $content ?>
	</div>
</body>

<?php include_once 'footer.php'; ?>
</html>