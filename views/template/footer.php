<?php
/**
     * Fichier footer.php - Correcteur de formulaire pour l'ENT ECLAT (KOSMOS)
     * Réalisé par Y. VEILLET
     * Développement Initial par Fabien CABANEL
     * Architecture MVC inspirée du LIVE CODING : https://nouvelle-techno.fr/actualites/live-coding-introduction-au-mvc-en-php
*/
?>
<div class="footer">
<hr class="hr-<?php echo $_GET["action"]; ?>"><p>Réalisé par Y. VEILLET à partir du travail de Fabien CABANEL</p>
<br/>
</div>