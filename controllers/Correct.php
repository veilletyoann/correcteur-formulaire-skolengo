<?php
/**
     * Fichier Correction.php - Correcteur de formulaire pour l'ENT ECLAT (KOSMOS)
     * Réalisé par Y. VEILLET
     * Développement Initial par Fabien CABANEL
     * Architecture MVC inspirée du LIVE CODING : https://nouvelle-techno.fr/actualites/live-coding-introduction-au-mvc-en-php
*/

/*
 * Class Correction
*/

class Correct extends Controller
{
    /*******************************************************************************************
	 * AFFICHE LA VUE CORRECTION.HTML LORS DE L'ACTION CORRECTION DEPUIS INDEX.PHP
	 *******************************************************************************************/
		
	public static function actionCorrect()
	{
		// Verification Passage par la page parameter
		if(($_POST['nomfichier']) && (isset($_SESSION['tableau'])))
		{
			Controller::displayHTML('correct');
		}
		else 
		{
			header('Location: index.php?action=load');
		}
    }
	
	/*******************************************************************************************
	 * CORRIGE LE FICHIER ET CREER LE CSV
	 *******************************************************************************************/
	
	public static function correct_csv()
	{
		$nomfichier=$_POST['nomfichier'];
		$tableau=$_SESSION['tableau'];
		$nblignes = count($tableau);
		$nbcolonnes = count($tableau[0])-1;
		$bonnes_reponses = array();
		$points = array();
		$total_points=0;

		/*
		// Suppresion de la ligne entete pour le tri
		$entete=array_shift($tableau);
		$tableau = array_merge($tableau);
		// Tri par Répondants
		for($i = 0;  $i<$nblignes-1; $i++)
		{
			$names[]= $tableau[$i][2];
		}
		array_multisort($names, $tableau);
		// Ajout de l'entête après tri
		array_unshift($tableau,$entete);
		*/
		
		// Init pour couleur des cases
		$j=0;
		for($i = 0;  $i<$nblignes+1; $i++)
		{
			if($tableau[$i][2]=="Répondants"){
			$j=$i;
		}
		
		// Récupére les réponses acceptées comme bonnes, données dans la méthode post depuis l'action parameters
		$ptmax='';
		for ($j = 3; $j<$nbcolonnes; $j++)
		{
			${'ptmax'.$j}=0;
			
			for($i = 1;  $i<$nblignes+1; $i++)
			{
				if (!isset($tableau[$i][$j])) 
				{
					$points[$i][$j]=0;
				}
		
				if (isset($_POST["nombre" . $i . "_" . $j])) 
				{
					if (isset($_POST['reponse'.$i.'_'.$j]))
					{
						$bonnes_reponses[$i][$j]=stripslashes($_POST['reponse'.$i.'_'.$j]);
						if ($_POST['reponse'.$i.'_'.$j] == "choix_perso")
						{
							$bonnes_reponses[$i][$j] = $_POST['choix_perso'.$j]; 	
						}
				
				
						//Vérifier s'il s'agit de La bonne réponse, c'est à dire celle qui a le plus de points.
						if ($_POST["nombre" . $i . "_" . $j] >= ${'ptmax'.$j} ) 
						{
							${'ptmax'.$j}=$_POST["nombre" . $i . "_" . $j];
							${'lareponse'.$j} = $bonnes_reponses[$i][$j];
						}
								
					}	
					$points[$i][$j]=$_POST["nombre" . $i . "_" . $j];
				}
				else 
				{
					//$_POST["nombre" . $i . "_" . $j]=0;  
					if (${'ptmax'.$j}==0) 
					{
						${'lareponse'.$j}='';
					}	
				}
			}
			$ptmax=$ptmax.'@@'.${'ptmax'.$j};			
		}
	
		// Ajout des points dans le tableau
		for ($i = 1; $i < $nblignes; $i++)
		{
			for ($j = 3; $j < $nbcolonnes; $j++)
			{
				$points_complets[$i][$j]=0;
				for ($k = 1 ; $k<$nblignes+1 ; $k++)
				{ 
					if ( isset ($bonnes_reponses[$k][$j]) )
					{
						if ($tableau[$i][$j] == $bonnes_reponses[$k][$j] ) 
						{
							$points_complets[$i][$j]=$points[$k][$j];
						} 
					}
				}
			}
		}

		// Comptage des points des utilisateurs
		$tableau[0][1] = 'Points';
		for ($i = 1; $i < $nblignes; $i++)
		{
			${'pt_individu'.$i}=0;
			for ($j = 3; $j < $nbcolonnes; $j++)
			{
				${'pt_individu'.$i} = ${'pt_individu'.$i} + $points_complets[$i][$j];
			}
			$tableau[$i][1] = ${'pt_individu'.$i};
		}
	
		// Comptage du total de points distribués
		$total_points=0;
		for ($j = 3; $j < $nbcolonnes; $j++)
		{
			$total_points = $total_points + ${'ptmax'.$j} ;
		}


		//Insertion des bonnes réponses dans le tableau final
		$tableau[$nblignes][0] = 'Note maximum';
		$tableau[$nblignes][1] = ''.$total_points.'';

		$tableau[$nblignes][2] = 'Réponses attendues';
		for($j=3;$j<$nbcolonnes;$j++)  $tableau[$nblignes][$j] = ${'lareponse'.$j} ;

		//Insertion du taux de réussite pour chaque question
		$tableau[$nblignes+1][0] = 'Participants';
		$nb_participants=$nblignes-1;
		$tableau[$nblignes+1][1] = ''.$nb_participants.'';
		
		// Affichage et calcul du taux de réussite pour chaque question
		$tableau[$nblignes+1][2] = 'Taux de réussite';
		for ($j = 3; $j < $nbcolonnes; $j++)
		{
			$tx=0;
			for ($i = 1; $i < $nblignes; $i++)
				{if ((isset($points_complets[$i][$j])) AND ($points_complets[$i][$j]!=0))  $tx++; }
				$taux=ceil($tx*100/($nblignes-1)); 
				$tableau[$nblignes+1][$j] = $taux.' %';
		}

		}
		
		// Session pour download csv
		$_SESSION['tableau_export_csv']=$tableau;
		
		// Retour des valeur pour affichage du tableau dans la vue correction.php
		return array($tableau,$nomfichier,$nblignes,$nbcolonnes,$points_complets,$ptmax,$total_points);
	}
}
?>