<?php
/**
     * Fichier Download.php - Correcteur de formulaire pour l'ENT ECLAT (KOSMOS)
     * Réalisé par Y. VEILLET
     * Développement Initial par Fabien CABANEL
     * Architecture MVC inspirée du LIVE CODING : https://nouvelle-techno.fr/actualites/live-coding-introduction-au-mvc-en-php
*/

/*
 * Class Download
*/

class Download extends Controller{
    
	/*******************************************************************************************
	 * LANCE LE TELECHARGEMENT DU FICHIER CSV LORS DE L'ACTION DOWNLOAD DEPUIS INDEX.PHP
	 * OU REDIRIGE SUR INDEX.PHP AVCE L'ACTION LOAD
	 *******************************************************************************************/
	
	public static function actionDownload()
	{
       	if ((isset($_POST["filename"])) && (isset($_SESSION['tableau_export_csv'])))
		{
			// Init des variables POST
			$tableau=$_SESSION['tableau_export_csv'];
			$filename='Correction_'.$_POST["filename"].'.csv';
			$file=DATA.$filename;
			
			//Génére le fichier csv corrigé
			$fp = fopen($file, 'w');

			foreach ($tableau as $fields) 
			{
				fputcsv($fp, array_map('utf8_decode',array_values($fields)), ';', '"');
			}
			fclose($fp);
						
			// Téléchargement du CSV
			header("Content-type: text/csv");
			header("Content-disposition: attachment; filename = ".$filename."");
			if (file_exists($file))
			{
				readfile($file);
				unlink($file);
			}
		}
		else
		{
			header('Location: index.php?action=load');
		}
	}
}
?>