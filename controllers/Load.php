<?php
/**
     * Fichier Controller.php - Correcteur de formulaire pour l'ENT ECLAT (KOSMOS)
     * Réalisé par Y. VEILLET
     * Développement Initial par Fabien CABANEL
     * Architecture MVC inspirée du LIVE CODING : https://nouvelle-techno.fr/actualites/live-coding-introduction-au-mvc-en-php
*/

/*
 * Class Load
*/

class Load extends Controller{
   
   /*******************************************************************************************
	 * AFFICHE LA PAGE LOAD.HTML LORS DE L'ACTION LOAD DEPUIS INDEX.PHP
	 *******************************************************************************************/
	
	public static function actionLoad()
	{
    	// Détruit toutes les variables de session
		$_SESSION = array();

		// Destruction du cookie de session.
		if (ini_get("session.use_cookies")) 
		{
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000,
			$params["path"], $params["domain"],
			$params["secure"], $params["httponly"]);
		}

		// Finalement, on détruit la session.
		session_destroy();
		
		Controller::displayHTML('load');
    }
}
?>