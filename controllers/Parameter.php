<?php
/**
     * Fichier Read.php - Correcteur de formulaire pour l'ENT ECLAT (KOSMOS)
     * Réalisé par Y. VEILLET
     * Développement Initial par Fabien CABANEL
     * Architecture MVC inspirée du LIVE CODING : https://nouvelle-techno.fr/actualites/live-coding-introduction-au-mvc-en-php
*/

/*
 * Class Read
*/

class Parameter extends Controller{
    
	/*******************************************************************************************
	 * AFFICHE LA VUE READ.HTML LORS DE L'ACTION LOAD DEPUIS INDEX.PHP
	 *******************************************************************************************/
	
	public static function actionParameter()
	{
		if(isset($_FILES["zip_file"]["name"]))
		{
			Controller::displayHTML('parameter');
		}
		else
		{
			header('Location: index.php?action=load');
		}
    }
	
	/*******************************************************************************************
	 * SEPARATION NOM PRENOM ET NORME RGPD
	 *******************************************************************************************/
	
	public static function extraire_nom_prenom($string) 
	{
		// Suppression des caractères avec accents
		$search  = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ');
		$replace = array('A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y');
		$string = str_replace($search, $replace, $string);
		//var_dump($string);
				
		// Traitement des NOMS, NOMS COMPOSES ET PRENOMS
		$parts = explode(" ", $string);
		//var_dump($parts);
		//var_dump(count($parts));
		if(count($parts) > 1) 
		{
			$firstname = array_pop($parts);
			$lastname = implode(" ", $parts);
		}
		else
		{
			$lastname = $string;
			$firstname = " ";
		}
		
		//var_dump($lastname);
		//var_dump($firstname);
		
		$RGPD_nom= substr($lastname, 0, 4);
		//var_dump($RGPD_nom);
		$RGPD_prenom= substr($firstname, 0, 4);
		//var_dump($RGPD_prenom);
		$RGPD=$RGPD_nom.' '.$RGPD_prenom;
		return $RGPD;
		
		/*
		if (preg_match("#((?:\b[[:upper:]'\s-]+\b)+)\s+((?:\b[[:upper:]][[:lower:]'\s-]+\b)+)#", $string, $m)) 
		{
			$RGPD_nom= substr($m[1], 0, 4);
			//var_dump($RGPD_nom);
			$RGPD_prenom= substr($m[2], 0, 4);
			//var_dump($RGPD_prenom);
			$RGPD=$RGPD_nom.' '.$RGPD_prenom;
			return $RGPD;
		}
		*/
		
			
		//return FALSE;
	}
	
	/*******************************************************************************************
	 * COPIE LE FICHIER DANS DATA, RENOMME LE CSV/ZIP, EXTRAIT L'ARCHIVE ZIP
	 *******************************************************************************************/
	
	private static function unzip($filename,$source)
	{
			$extension_file=strtolower(substr($filename,-3));
			//var_dump($extension_file1);
			
			$name = explode(".".$extension_file, $filename);
			//var_dump($name1[0]);
			//$name = explode(".", $filename);
			
			//if((strtolower($name[1]) == 'zip') || (strtolower($name[1]) == 'csv'))
			if (($extension_file == 'zip') || ($extension_file == 'csv'))
			{
				$continue = true;
				//$extension_file = strtolower($name[1]);
			}
			else 
			{
				header('Location: index.php?action=load');
			}
			
			if($continue==true) 
			{
				if(move_uploaded_file($source, DATA.$filename)) 
				{
					$tmp_name='formulaire'.rand(1,1000);
					
					// Renomme et extrait le ZIP
					if ($extension_file == 'zip')
					{
						// Suppression des caractères avec accents
						$search  = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ');
						$replace = array('A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y');
						$filename = str_replace($search, $replace, $filename);
						
						/*On supprime les éventuels caractères  spéciaux et majuscules
						$filename = strtr($filename,"ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ","AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn");
						*/
						
						//$filename = strtolower($filename);
                
						rename(DATA.$filename, DATA.$tmp_name.'.'.$extension_file);
						$zip = new ZipArchive();
						$x = $zip->open(DATA.$tmp_name.'.'.$extension_file);
						if ($x === true)
						{
							$zip->extractTo(DATA);
							
							// Récupération du nom du fichier csv extrait et vérification de l'extension
							for ($i = 0; $i < $zip->numFiles; $i++)
							{
								if(strtolower(substr($zip->getNameIndex($i),-3)) == 'csv')
								{
									$csv_name = $zip->getNameIndex($i);
									//echo $csv_name;
								}
							}
							
							$zip->close();
							rename(DATA.substr($csv_name,0,-4).'.csv',DATA.$tmp_name.'.csv');
							//rename(DATA.substr($filename,0,-4).'.csv',DATA.$tmp_name.'.csv');
							unlink(DATA.$tmp_name.'.zip');
						}
					}
					// Renomme le fichier csv
					if ($extension_file == 'csv')
					{
						rename(DATA.$filename, DATA.$tmp_name.'.'.$extension_file);
					}
										
					//$_SESSION['nomfichier'] = $tmp_name;
					return $tmp_name;
				} 
				else
				{    
					$message = "Erreur de chargement de fichier !";
				}
				
			}
	}
	
	
	/*******************************************************************************************
	 * TRAITE LE FICHIER CSV POUR CREER LE TABLEAU
	 *******************************************************************************************/
	
	public static function file_processing()
	{
		if($_FILES["zip_file"]["name"])  
		{
			$tmp_name=self::unzip($_FILES["zip_file"]["name"],$_FILES["zip_file"]["tmp_name"]);	
		}

	$fichier = DATA.$tmp_name.'.csv';
			
	$tableau = array();
	$numero_ligne = 0;
	if (($handle = fopen($fichier, "r")) !== FALSE)
	{
		while (($tableau[$numero_ligne] = fgetcsv($handle, 10000, ";")) !== FALSE) 
		{
			$numero_ligne++; 
		}
		fclose($handle);
	}
	
	// Suppression de la dernière ligne du tableau (vide ?)
	array_pop($tableau);
	$tableau = array_merge($tableau);
	
	//$nblignes = count($tableau)-1;
	$nblignes = count($tableau);
	$nbcolonnes = count($tableau[0]);
	

	// UTF8
	if (!mb_check_encoding(file_get_contents($fichier), 'UTF-8'))
	{
		for ($i=0; $i<$nblignes ; $i++)
		{
			for ($j=0; $j<$nbcolonnes ; $j++)
			{
				$tableau[$i][$j] = utf8_encode($tableau[$i][$j]);
			}
		}
	}
	
	// Suppression de la ligne entete pour le tri
	$entete=array_shift($tableau);
	$tableau = array_merge($tableau);
	// Array Répondants
	for($i = 0;  $i<$nblignes-1; $i++)
	{
		$names[]= $tableau[$i][1];
	}
	// Tri par Répondants
	array_multisort($names, $tableau);
	// Ajout de l'entête après tri
	array_unshift($tableau,$entete);
		
	
	// Norme RGPD : trois premières lettre du Nom et Prénom des Répondants.
	for ($i=1; $i<$nblignes ; $i++)
	{
			for ($j=0; $j<$nbcolonnes ; $j++)
			{
				if ($j==1)
				{
					$tableau[$i][$j]=self::extraire_nom_prenom($tableau[$i][$j]);
					//var_dump($tableau[$i][$j]);
				}
			}
	}

	// Insertion case vide à la fin des lignes suite modif formulaire (; absent à la fin de la ligne)
	for ($i=0; $i<$nblignes ; $i++)
	{
		if ($tableau[$i][$nbcolonnes - 1] != "")
		{
			$tableau[$i][$nbcolonnes] = "";
		}	
	}
	//$nblignes = count($tableau)-1;
	$nblignes = count($tableau);
	$nbcolonnes = count($tableau[0]);
	unlink ($fichier);

	// Vérification des cases (pas de case non remplie)
	for ($i=0; $i<$nblignes ; $i++)
	{
			for ($j=0 ; $j<$nbcolonnes-1 ; $j++)
			{
				if ($tableau[$i][$j]=='') {$tableau[$i][$j]=' ';}
			}
	}

	// Insértion d'une colonne "nombre de points" dans le tableau: la colonne est placée en 2nd position (donc j=1) et est la copie de la colonne j=0 (colonne DATE)
	for ($i=0; $i<$nblignes ; $i++)
	{
		for ($j=$nbcolonnes ; $j>0 ; $j--)
		{
			$tableau[$i][$j]=$tableau[$i][$j-1];
		}
	}
	unset($tableau[$nblignes]);

	// Création d'un tableau contenant l'intitulé des questions et les réponses différentes données par les répondants
	$tableau_reponses_differentes = array();

	for ($j=3 ; $j<$nbcolonnes ; $j++) 
	{
		for ($i=0; $i<$nblignes ; $i++)
		{
			if ($i<2)
			{
				$tableau_reponses_differentes[$i][$j] = $tableau[$i][$j];
			}
			if ($i>1)
			{
				$flag=0;
				for ($k=1 ; $k<$i ; $k++) 
				{
					if ($tableau[$i][$j] == $tableau_reponses_differentes[$k][$j])
					{
						$flag=1; $tableau_reponses_differentes[$i][$j]="DEJA_DIT";
					}
				}
				if ($flag==0) 
				{
					$tableau_reponses_differentes[$i][$j] = $tableau[$i][$j];
				}
			}
		}
		//else $tableau_reponses_differentes[$i][$j]="DEJA_DIT";
	}
	
	// Mise en session du tableau pour correction
	$_SESSION['tableau'] = $tableau;
	// Retour pour affichage dans la vue read.php
	return array($tableau,$tableau_reponses_differentes,$tmp_name);
	}
}
?>