<?php
/**
     * Fichier Controller.php - Correcteur de formulaire pour l'ENT ECLAT (KOSMOS)
     * Réalisé par Y. VEILLET
     * Développement Initial par Fabien CABANEL
     * Architecture MVC inspirée du LIVE CODING : https://nouvelle-techno.fr/actualites/live-coding-introduction-au-mvc-en-php
*/

/*
 * Controleur principal
*/
    
abstract class Controller{
    
	/*******************************************************************************************
	 * AFFICHE LA PAGE HTML PAR $fichier
	 *******************************************************************************************/
	
	public static function displayHTML($fichier)
	{ 
		// Démarrage du buffer de sortie
        ob_start();

        // Création de la vue
        require_once(ROOT.'views/'.$fichier.'.php');

        // Stockage du contenu dans $content
        $content = ob_get_clean();

        // Fabrication du "template"
        require_once(ROOT.'views/template/default.php');
    }
}
?>