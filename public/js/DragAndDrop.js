/**
     * Fichier dd.js - Correcteur de formulaire pour l'ENT ECLAT (KOSMOS)
     * Réalisé par Y. VEILLET
     * Développement Initial par Fabien CABANEL
     * Architecture MVC inspirée du LIVE CODING : https://nouvelle-techno.fr/actualites/live-coding-introduction-au-mvc-en-php
*/
		
/* Zone de dépot : depose */
var depose=document.getElementById("depose");

/* Gestion du clic sur la zone depose*/
depose.addEventListener("click", function(evt) 
{
  evt.preventDefault();
  /* Simule le clic sur l'imput zip_file */
  document.getElementById("zip_file").click();
});

/* Gestion du DRAG AND DROP dans la zone depose */
depose.addEventListener("dragover", function(evt) 
{
  /* Autorise le drop par JS */
  evt.preventDefault(); 
});

depose.addEventListener("dragenter", function(evt) 
{
  /* Surbrillance ajoutée */
  this.className="onDropZone"; 
});

depose.addEventListener("dragleave", function(evt) 
{
  /* Surbrillance supprimée */
  this.className=""; 
}); 

depose.addEventListener("drop", function(evt) 
{
  evt.preventDefault();
  /* Tranfert de la liste des fichiers du drag and drop dans input zip_file */
  document.getElementById("zip_file").files=evt.dataTransfer.files;
  /* Surbrillance supprimée */
  this.className=""; 
  /* Appel de la fonction */
  deposeFileZip();
});

document.getElementById("zip_file").addEventListener("change", function(evt)
{
  deposeFileZip();
});

/* Actions lors de la sélection/dépot du fichier zip */
function deposeFileZip()
{
	/* Bloc d'affichage de la liste des fichiers */
	var p=document.getElementById("preview");
	/* Effacer le contenu initial de #preview */
	p.innerHTML=""; 
	/* Affichage du fichier dans #preview */
	var f=document.getElementById("zip_file").files[0];
    var div=document.createElement("div");
    div.className="fichier";
    var span=document.createElement("span");
    span.innerHTML=f.name+" ("+getHumanSize(f.size)+")";
    var vignette=document.createElement("img");
    vignette.setAttribute("src", "public/images/zip.png");
    /* Attacher les élements HTML au DOM */
    div.appendChild(vignette);
    div.appendChild(span);
    p.appendChild(div);
	/* Modification du display #preview */
	p.style.display="block";
	/* Modification du display #depose */
	depose.style.display="none";
	/* Modification du display #action_buttons */
	document.getElementById("action_buttons").style.display="block";
}

/* Retourne une taille de fichier en mode lisible par un humain */
function getHumanSize(s) 
{
  /* Pour s'assurer que le paramètre d'entrée est entier */
  s=parseInt(s); 
  if (s<1024) {
    return s+" o";  
  } else if (s<1024*1024) {
    return (s/1024).toFixed(1)+" ko";  
  } else if (s<1024*1024*1024) {
    return (s/1024/1024).toFixed(1)+" Mo";  
  } else {
    return (s/1024/1024/1024).toFixed(1)+" Go";  
  }
}