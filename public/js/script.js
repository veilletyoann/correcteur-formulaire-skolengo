/**
     * Fichier script.js - Correcteur de formulaire pour l'ENT ECLAT (KOSMOS)
     * Réalisé par Y. VEILLET
     * Développement Initial par Fabien CABANEL
     * Architecture MVC inspirée du LIVE CODING : https://nouvelle-techno.fr/actualites/live-coding-introduction-au-mvc-en-php
*/

//// Fonctions javascript des views HTML

// load.php : vérification du fichier avant envoi du formulaire
function controle_form()
{
	var input_file = document.forms['form_load_zip']['zip_file'].files[0];
	
	if(input_file == undefined) 
	{
		alert("Merci de sélectionnner un fichier");
		return false;
	}
	else
	{
		return true;
	}
}

// parameter.php : Ajout ou suppression de points (sens) dans l'imput text (zone) avec incrément (0.5 ou 1)
function Points(sens,zone,increment,idText) 
{
	
	// Test si le nombre da la zone est décimal (.)
	nbre_decimal=zone.value.indexOf('.');
		
	// Conversion string en nombre si le nombre de la zone est décimal
	if (nbre_decimal == -1) 
	{
		valeur=parseInt(zone.value, 10);
	}
	// Conversion string en nombre si le nombre de la zone est entier
	if (nbre_decimal == 1)
	{
		valeur=parseFloat(zone.value);
	}
		
	// Action si sens = Plus
	if (sens=='Plus')
	{
		valeur+=increment;
	}
	// Action si sens = Moins
	if (sens=='Moins')
	{
		valeur-=increment;
	}
	
	// Couleurs
	if (valeur == 0) 
	{
		zone.style='color:black;';
		document.getElementById('span_reponse'+idText).style='color:black;';
	}
	if (valeur < 0) 
	{
		zone.style='color:red;';
		document.getElementById('span_reponse'+idText).style='font-weight:bold;color:red;';
	}
	if (valeur > 0) 
	{
		if(isDecimal(valeur)==true)
		{
			zone.style='color:orange;';
			document.getElementById('span_reponse'+idText).style='font-weight:bold;color:orange;';
		}
		else
		{
			zone.style='color:green;';
			document.getElementById('span_reponse'+idText).style='font-weight:bold;color:green;';
		}
	}
	
	// Ecriture de la valeur dans la zone
	zone.value=valeur;
}

function isDecimal( iNumber ) {
return iNumber % 1 !== 0;
}

// parameter.php et correction.php : redirection ou redirection et téléchargement
function redir(page,message)
{
	if (message=='Yes')
	{
		var choix = confirm("Voulez vous quitter la page et retourner à l'accueil ?");
		if (choix == true)
		{
			document.location.href=page;
		}
	}
	if (message=='No')
	{
		document.location.href=page;
	}
	
}

function ExportPdf(filename){ 
 
	kendo.drawing
    .drawDOM("#table-pdf-printable",
    { 
        paperSize: "A3",
		landscape: true,
		margin: { top: "1cm", bottom: "1cm" },
        scale: 0.4,
        height: 297
    })
		
        .then(function(group){
        kendo.drawing.pdf.saveAs(group, "Correction_"+filename+".pdf")
    });
}