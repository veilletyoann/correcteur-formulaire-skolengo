<?php
/**
     * Fichier index.php - Correcteur de formulaire pour l'ENT ECLAT (KOSMOS)
     * Réalisé par Y. VEILLET
     * Développement Initial par Fabien CABANEL
     * Architecture MVC inspirée du LIVE CODING : https://nouvelle-techno.fr/actualites/live-coding-introduction-au-mvc-en-php
*/

// Initialisation de la session.
session_set_cookie_params(0);
session_start();

// LANG et UTF8
putenv("LANG=fr_FR.UTF-8");
setlocale(LC_ALL, 'fr_FR.UTF-8');

// Définition des constantes depuis la racine publique du projet
define('ROOT', str_replace('index.php','',$_SERVER['SCRIPT_FILENAME']));
define('DATA', ROOT."data/");
define('PUBLIC', ROOT."public/");

// Appel du contrôleur principal
require_once(ROOT.'app/Controller.php');

// GET action dans $params
$params = $_GET['action'];

// Chargement des controllers et pages si action load - read - correction - download
if(($params == "load") || ($params == "parameter") || ($params == "correct") || ($params == "download"))
{
    // Sauvegarde du paramètre dans $controller en mettant sa 1ère lettre en majuscule
    $controller = ucfirst($params);
	
	// Appel du contrôleur correspondant au paramètre GET
    require_once(ROOT.'controllers/'.$controller.'.php');

    // Définition de la méthode du controleur par défaut;
	$action = 'action'.ucfirst($params);

    // Démarrage de l'instance du contrôleur
    $controller = new $controller();
	
	if(method_exists($controller, $action))
	{
        // Appel de la méthode
		$controller::$action();
		//$controller->$action();
	}
	else
	{
        // Envoi du code réponse 404
        http_response_code(404);
        echo "Cette page n'existe pas";
	}
}
else
{
		// Redirection si aucun paramètre n'est défini
		header('Location: index.php?action=load');
}
?>
